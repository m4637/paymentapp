const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

// Imports to manage user sessions
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const { initialize } = require('./server/conf/passportSetup');

// Use express layout to manage ejs files
const expressLayouts = require('express-ejs-layouts');

// Environment variables
const env = require("dotenv").config({ path: "./.env" });
const sessionParams = require('./server/conf/expressSession');


// Development library to see objects content
const util = require('util');
const c = require('./server/conf/colors');

// Data security libraries
const bcrypt = require('bcrypt');
const CryptoJS = require("crypto-js");

// Database & Models
const mongoose = require('mongoose');
const UserSchema = require('./server/models/UserModel');


// Map routes
const mainRoute = require('./server/routes/mainRoute');
const userRoute= require('./server/routes/userRoute');
const stripeRoute = require('./server/routes/stripeRoute');
const devRoute = require('./server/routes/devRoute');
const productRoute = require('./server/routes/productRoute');


const { user_address } = require('./server/controller/databaseController.js');

// Run the server and initialize sessions
const app = express();
initialize(passport);

// Connect to MongoDB
mongoose.connect(process.env.DB_PASSWORD, { useNewUrlParser: true, useUnifiedTopology: true})
.then(() => console.log(c.iOrange + "[+] Listening database on cluster" + c.reset))
.catch(err => console.log(c.fRed + err + c.reset));


// Get the last user inserted salt and decrypt it
UserSchema.find().sort({_id:-1}).limit(1).exec(function(err, user) {
    if(!err && user[0].salt != undefined) {
        console.log('[-] Salt before ' + user[0].salt);
        var bytes  = CryptoJS.AES.decrypt(user[0].salt, process.env.AES_SECRET_KEY);
        var originalText = bytes.toString(CryptoJS.enc.Utf8);
        console.log('[-] Salt after ' + originalText);
    } 
    else { console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); }
});


// Set the port in environment file for development
// If the environment file is missing start the server
// on port 3000 by default
const PORT = process.env.PORT || 3000;

// Used to parse the response for data extraction
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// View Engine Setup 
app.set('views', path.join(__dirname, 'public/views'));
app.set('view engine', 'ejs');
// Redirect on layout by default
app.use(expressLayouts);
// Prevent layout on pages
app.set("layout invoice", false);


// Use Express session properties
app.use(session(sessionParams.SESSION));

// Use passport session
app.use(passport.initialize());
app.use(passport.session());

// Connect flash middleware that returns
// flash messages during registration / connection
// Also used for layout view
app.use(flash());

// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.warning_msg = req.flash('warning_msg');
    res.locals.error_msg   = req.flash('error_msg');
    res.locals.error       = req.flash('error');
    next();
});


// Set Public folder /!\ Don't forget the first '/'
app.use('/public', express.static(path.join(__dirname, '/public')));

// Make reference to application routes
app.use('/', mainRoute);
app.use('/', devRoute);
app.use('/', stripeRoute);
app.use('/', userRoute);
app.use('/product', productRoute);

// Listen for connections on the server
app.listen(PORT, () => console.log(c.iOrange + `[+] Server listening on port ${PORT} !` + c.reset));