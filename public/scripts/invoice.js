window.onload = function () { 
	$('#printInvoice').click(function(){
		Popup($('.invoice')[0].outerHTML);
		function Popup(data) 
		{
			window.print();
			return true;
		}
	  });

	$('#pdfInvoice').click(function(){
	// Choose the element that our invoice is rendered in.
	const element = $('.invoice')[0].outerHTML

	// Choose the options 
	var options = {
		filename : 'invoice.pdf'
	}

	// Choose the element and save the PDF for our user.
	html2pdf().set(options).from(element).save();
	});
}

