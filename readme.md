# Mario & Luigi WEBSITE : README

## Project

<br>

This project is an Ynov  project from software architecture module which consist in a marketplace creation after ABC overhaul.
We need to implement all customer services and technical documents as SAV, Basket, Payment module, Cross and Up selling, etc.

For this project we have chosen to use an EJS template engine based on a node js backend.

<br>

## Project Goals

<br>

To achieve our objective we will have to carry out several tasks provided by the specifications accessible on https://trello.com/invite/b/RRAaA6td/cbf1fd230541f57b5eac530cab891e3e/payment-app Trello dashboard.

<br>

## INSTALLATION

<br>

To install and run the project you may be sure that you have on your environment all pre requires.
* [...]  Node js is installed https://nodejs.org/en/ *check if you have the **lates stable version LTS** long term support.
You can verify node installation by typing on a cmd located in the installation folder, unless you have parameter environment variables to run this command from everywhere **node --version**
* [...] Get the project url and clone it https://gitlab.com/m4637/paymentapp
    * git clone https://gitlab.com/m4637/paymentapp.git
* [...] In the project folder, in a cmd do **npm install** to download all project dependencies and libraries
* [...] Launche the project **node server.js** you can run **npm run persist** to use nodemon extension that reload automatically after update on the project or **npm run start** or **node index.js**.
* [...] From your navigator enter **localhost:port**.

* [...] If the project is already deployed check shared heroku link.

<br>


## AUTHORS

<br>

Name | Abilities & Tasks | LinkedIn
------------ | ------------- | -------------
```Cerdan Guillaume``` | Product design and CDC | https://fr.linkedin.com/in/guillaume-cerdan-794442200
```Flament Ludovic``` | Product design and CDC | https://fr.linkedin.com/in/ludovic-flament-985111183
```Hecquet Christian``` | Product design and CDC | https://fr.linkedin.com/in/christian-hecquet-978665178

<br>

![Guillaume Cerdan](./readme_locals/profile/Guillaume_CERDAN.png)
![Ludovic Flament](./readme_locals/profile/Ludovic_FLAMENT.png)
![Christian Hecquet](./readme_locals/profile/Christian_HECQUET.png)


<br>
<br>

## Links and references

<br>

### Project Management

<br>

* ```Gitlab (private) :``` https://gitlab.com/m4637/paymentapp
* ```Trello (by invite link) :``` https://trello.com/invite/b/RRAaA6td/cbf1fd230541f57b5eac530cab891e3e/payment-app
* ```Draw.io :``` https://app.diagrams.net/

<br>

### Technical references

<br>

#### Database

* ```MongoDB Doc :``` https://docs.mongodb.com/
* ```Mongoose Doc :``` https://mongoosejs.com/docs/

#### Server & Session

* ```Express Js :``` https://expressjs.com/fr/
* ```Passport js :``` http://www.passportjs.org/

#### Hosting

* ```MongoDB :``` https://account.mongodb.com/account/login
* ```Heroku (if time allows) :``` https://www.heroku.com/


<br>

### Credits

<br>

#### Image banks

* ```Flaticon :``` https://www.flaticon.com/
* ```Pixabay :``` https://pixabay.com/
* ```Pexels :``` https://www.pexels.com/fr-fr/
* ```Unsplash :``` https://unsplash.com/

#### Image authors

* ```HomePage Banner :``` Olivier Piquer, https://unsplash.com/@olivpi?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
* ```Team Banner :``` Sidekix Media, https://unsplash.com/@sidekix?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
* ```About chair :``` Davide Cantelli, https://unsplash.com/@cant89?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
* ```About plant :``` freddie marriage, https://unsplash.com/@fredmarriage?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
* ```Top arrow button :``` HAJICON, https://www.flaticon.com/authors/hajicon 
* ```Chair icon :``` ultimatearm, https://www.flaticon.com/authors/ultimatearm
* ```Plant from logo icon :``` surang, https://www.flaticon.com/authors/surang
* ```Footer Banner :``` Sidekix Media, https://unsplash.com/@hutomoabrianto?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
* ```Gallery images :``` Check the picture description.
