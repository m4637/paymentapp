exports.society_info = {
    name: 'Mario & Luigi père et fils'
};

exports.society_address = { 
    line1: 'Avenue des lutins de lumière', 
    postal_code: '85050', 
    city: 'Castelgrande', 
    state: 'Potenza (PZ)', 
    country: 'Italia', 
};

exports.user_info = {
    name: 'Mario',
    last: 'Itsumi'
};

exports.user_address = { 
    line1: 'Via Miguel de cervantes, 107', 
    postal_code: '85050', 
    city: 'Castelgrande', 
    state: 'Potenza (PZ)', 
    country: 'Italia', 
};

exports.products = {
    item_0: {   product_name: 'Princess peach plush',
                product_description: 'The peach princess from Mario\'s games, size: 25cm, color: orange and pink',
                product_id: '7cQhEDcsDJLDdCDj6XXPh8zd',
                product_price: 3000},
    item_1: {   product_name: 'Flag from Mario',
                product_description: 'It\'s the finish flag from Mario\'s games, size: 126cm, color: grey, note: Not for children',
                product_id: '6MEtJMwVdNm3GuDqjL2BFUV2',
                product_price: 6000},
    item_2: {   product_name: 'Dynamite',
                product_description: 'An explosive bar of dynamite to keep away troubles',
                product_id: 's6v3SujXjCXzGECbbXNwTHbu',
                product_price: 9000}
};
