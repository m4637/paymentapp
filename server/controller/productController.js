exports.checkFieldsUpdateProduct = fields => {
    // set the list of errors that will be returned to the front
    let errors = [];
    // Check that all required fields are filled for a security usage
    // to prevent man in the middle attacks
    if (fields.name == '' || fields.description == '' || fields.price == '') {
        errors.push({ msg: 'Fields can\'t be empty' });
    }

    return errors;
};


exports.checkFieldsCreateProduct = fields => {
    // set the list of errors that will be returned to the front
    let errors = [];
    
    // Check that all required fields are filled for a security usage
    // to prevent man in the middle attacks
    if (!fields.name || !fields.description || !fields.price) {
        errors.push({ msg: 'Please complete all required fields' });
    }

    // Check if the product name has a length over 6 characters for a 
    // secure usage
    if (fields.name.length < 6) {
        errors.push({ msg: 'Product name must be at least 6 characters' });
    }

    return errors;
};