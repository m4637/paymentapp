// Map controllers
const { products } = require('./databaseController.js');

const c = require('../conf/colors');

exports.calculateOrderAmount = product_id => {
    // Replace this constant with a calculation of the order's amount
    // You should always calculate the order total on the server to prevent
    // people from directly manipulating the amount on the client

    console.log('[-] pid_' + product_id);
    switch (product_id) {
        case products.item_0.product_id:
            
            calculatedProduct = {
                pid: product_id,
                product_name: products.item_0.product_name,
                orderAmount: products.item_0.product_price,
                product_desc: products.item_0.product_description
            };
           
            return calculatedProduct;
        case products.item_1.product_id:

            calculatedProduct = {
                pid: product_id,
                product_name: products.item_1.product_name,
                orderAmount: products.item_1.product_price,
                product_desc: products.item_1.product_description
            };
           
            return calculatedProduct;
        case products.item_2.product_id:

            calculatedProduct = {
                pid: product_id,
                product_name: products.item_2.product_name,
                orderAmount: products.item_2.product_price,
                product_desc: products.item_2.product_description
            };
           
            return calculatedProduct;
        default:
            console.log(c.fYellow + `Oups ¯\\_(ツ)_/¯, error with ` + product_id + c.reset);
            return null;
    }

};