exports.checkFieldsRegistration = fields => {
    // set the list of errors that will be returned to the front
    let errors = [];
    
    // Check that all required fields are filled for a security usage
    // to prevent man in the middle attacks
    if (!fields.firstName || !fields.lastName || !fields.email || !fields.confirmEmail ||
        !fields.addressLine || !fields.addressPostalCode || !fields.addressCity ||
        !fields.addressCountry || !fields.password || !fields.confirmPassword) {
        errors.push({ msg: 'Please complete all required fields' });
    }

    // Check if emails match before save for a good registration
    if (fields.email != fields.confirmEmail) {
        errors.push({ msg: 'Emails do not match' });
    }

    // Check if passwords match before save for a good registration
    if (fields.password != fields.confirmPassword) {
        errors.push({ msg: 'Passwords do not match' });
    }

    // Check if the password had a length over 6 characters for a 
    // secure usage
    if (fields.password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters' });
    }

    // TODO
    // to make a better field controller we may add regexp verification

    return errors;
};

exports.checkUserInfo = fields => {
    // set the list of errors that will be returned to the front
    let errors = [];
    
    // Check that all required fields are filled for a security usage
    // to prevent man in the middle attacks
    if (!fields.first_name || !fields.last_name) {
        errors.push({ msg: 'Please complete all required fields' });
    }

    if (!fields.email) {
        errors.push({ msg: 'Internal data error' });
    }

    // TODO
    // to make a better field controller we may add regexp verification

    return errors;
};

exports.checkUserAddress = fields => {
    // set the list of errors that will be returned to the front
    let errors = [];
    

    // Check that all required fields are filled for a security usage
    // to prevent man in the middle attacks
    if (!fields.addressLine1 || !fields.postalCode || 
        !fields.city || !fields.country) {
        errors.push({ msg: 'Please complete all required fields' });
    }

    if (!fields.email) {
        errors.push({ msg: 'Internal data error' });
    }

    // TODO
    // to make a better field controller we may add regexp verification

    return errors;
};

exports.checkUserAccount = fields => {
    // set the list of errors that will be returned to the front
    let errors = [];

    // Check that all required fields are filled for a security usage
    // to prevent man in the middle attacks
    if (!fields.newEmail || !fields.oldPassword) {
        errors.push({ msg: 'Please complete all required fields' });
    }

    // If we change password ensure that they are not null
    if(((fields.confirmNewPassword != '') && (fields.newPassword == '')) || 
    ((fields.newPassword != '') && (fields.confirmNewPassword == ''))) {
        errors.push({ msg: 'New password cannot be null' });
    }

    if (fields.newPassword != fields.confirmNewPassword) {
        errors.push({ msg: 'New passwords don\'t match' });
    }

    if ((fields.newPassword.length || fields.confirmNewPassword.length || fields.oldPassword.length) < 6) {
        errors.push({ msg: 'Passwords are too short' });
    }

    if (!fields.email) {
        errors.push({ msg: 'Internal data error' });
    }

    // TODO
    // to make a better field controller we may add regexp verification
 
     return errors;
};



