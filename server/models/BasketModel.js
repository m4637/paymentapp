const mongoose = require('mongoose');

const BasketSchema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true
    },
    products: [{ 
        type: String,
        required: true
    }],
    date: {
        type: Date,
        default: new Date().toString()
    },

});

const Basket = mongoose.model('baskets', BasketSchema);

module.exports = Basket;