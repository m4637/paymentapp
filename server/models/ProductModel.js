const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    cross_sell: [{ 
        type: String,
        required: true
    }]

});

const Product = mongoose.model('products', ProductSchema);

module.exports = Product;