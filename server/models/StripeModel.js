const mongoose = require('mongoose');

const StripeSchema = new mongoose.Schema({
transactionToken:{
    type: String,
    required: true
},
chargeId:{
    type: String,
    required: true
},
currency: {
    type: String,
    required: true
},
price: {
    type: Number,
    required: true
},
date: {
    type: Date,
    default: new Date().toString()
    }
});

const Stripe = mongoose.model('stripes', StripeSchema);

module.exports = Stripe;