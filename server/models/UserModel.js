const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
firstName: {
    type: String,
    required: true
},
lastName: {
    type: String,
    required: true
},
birth: { type: String },
email: {
    type: String,
    required: true
},
phone: { type: String },
mobile: { type: String },
address: {
    line: { 
        type: String,
        required: true
    },
    line2: { type: String },
    postalCode: {
        type: String,
        required: true
    }, 
    city: {
        type: String,
        required: true
    }, 
    state: { type: String },
    country: {
        type: String,
        required: true
    } 
},
password: {
    type: String,
    required: true
},
salt: {
    type: String,
    required: true
},
role: {
    type: Number,
    default: 1
},
date: {
    type: Date,
    default: new Date().toString()
    }
});

const User = mongoose.model('users', UserSchema);

module.exports = User;