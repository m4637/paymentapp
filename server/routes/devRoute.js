const express = require('express');
const router = express.Router();

// Get testing data
const { society_info, society_address, user_info, user_address } = require('../controller/databaseController.js');

// Layout invoice prevent layout style on invoice view
router.get('/invoice', function(req, res) {
    res.render('invoice', {
        layout: 'invoice',
        user_name: user_info.name,
        user_lastname: user_info.last,
        user_address: user_address,
        user_email: 'debug@gmail.com',
        product: {
            product_id: '7cQhEDcsKKLDdCDj6XXPh8zd',
            product_name: 'Some product',
            product_price: 8000 / 100,
            product_ht: (8000 / 100) / 1.055,
            product_tax: (8000 / 100 - (8000 / 100) / 1.055),
            product_desc: 'Some object'
        },
        society_address: society_address,
        society_info: society_info
    });
});


router.get('/error', function(req, res){
    res.render('Error');
});

module.exports = router;