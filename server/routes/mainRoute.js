const express = require('express');
const router = express.Router();
// Environment variables
const env = require("dotenv").config({ path: "./.env" });
const c = require('../conf/colors');

// Database & Models
const ProductSchema = require('../models/ProductModel');
const BasketSchema = require('../models/BasketModel');

// Get testing data
const { society_info, society_address, user_info, user_address, products } = require('../controller/databaseController.js');

// Listen for connection on '/' and return some data
// for display and logical use
router.get('/', function(req, res){ 
    res.render('Home', {
        navtype: req.user,
        user_key: process.env.Publishable_Key,
        user_name: user_info.name,
        user_lastname: user_info.last,
        user_address: user_address,
        item_0: products.item_0,
        item_1: products.item_1,
        item_2: products.item_2
    });
});

// Redirect to the FAQ application
router.get('/faq', function(req, res) {
    res.status(200).redirect('http://localhost:5001/');
});

// Get market
router.get('/market', (req, res) => {
    ProductSchema.find().sort({_id:-1}).exec(function(err, product) {
        if(!err) {
            res.render('Market', {
                navtype : req.user, 
                product
            });
        } 
        else { console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); }
    });
});


router.get('/basket', (req, res) => {
    const userId = req.user._id;

    BasketSchema.findOne({user_id: userId.toString() }).then(basket => {
        console.log(basket);
        ProductSchema.find({'_id': { $in: basket.products }}).exec(function(err, products) {
            if(products) {
                res.render('Basket', {
                    navtype: req.user,
                    basket: basket,
                    products: products
                });
            }
        });
    });
});


module.exports = router;