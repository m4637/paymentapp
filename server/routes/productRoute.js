const express = require('express');
const router = express.Router();

// Database & Models
const ProductSchema = require('../models/ProductModel');
const BasketSchema = require('../models/BasketModel');

// Environment variables
const c = require('../conf/colors');

// Map controllers
const { ensureAuthenticated } = require('../conf/passportSetup');
const { checkFieldsUpdateProduct, checkFieldsCreateProduct } = require("../controller/productController");


///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////


// Get all user products
router.get('/view', ensureAuthenticated, (req, res) => {
    ProductSchema.find({user_id: req.user._id}).exec(function(err, product) {
        if(!err) {
            res.render('ViewProducts', {
                navtype : req.user, 
                product: product
            });
        } 
        else { console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); }
    });
});

// Get the create product page
router.get('/create', ensureAuthenticated, (req, res) => {
    res.render('CreateProduct', {
        navtype: req.user
    });
});

// Get the update product page
router.get('/update/:id', ensureAuthenticated, (req, res) => {
    ProductSchema.findOne({_id: req.params.id}).exec(function(err, product) {
        product.cross_sell = product.cross_sell.join(', ');
        if(!err) {
            res.render('UpdateProduct', {
                navtype : req.user, 
                product: product
            });
        } 
        else { console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); }
    });
});

// Get a product detail
router.get('/:id', (req, res) => {
    ProductSchema.findOne({_id: req.params.id}).exec(function(err, product) {
        // product.cross_sell = product.cross_sell.join(', ');

        // TODO Remove dirty coding get one cross sell product instead of a list
        ProductSchema.find({cross_sell: product.cross_sell}).exec(function(err, crossSellList) {
            let crossSellProduct;
            let upSellProduct;
            crossSellList.forEach((crossSell, index) => {
                // Check if product is different
                if (!crossSell._id.equals(product._id)) {
                    if (parseInt(crossSell.price) > parseInt(product.price)) {
                        upSellProduct = crossSell;
                    } else {
                        crossSellProduct = crossSell; 
                    }
                }
            });
            if(!err) {
                res.render('Product', {
                    navtype : req.user, 
                    product: product,
                    crossSell: crossSellProduct,
                    upSell: upSellProduct
                });
            } 
            else { console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); }
        });
     
    });
});


///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

// Create a new product
router.post('/create', ensureAuthenticated, (req, res) => {
    const { name, price, description, productTag } = req.body;

    let productTagList = productTag.toLowerCase().replace(/ /g, '').split(',');

    let errors = checkFieldsCreateProduct(req.body);

    if (errors.length > 0) {
        res.render('CreateProduct', {
            errors,
            navtype: 'dashboard'
        });
    } else {

        const newProduct = new ProductSchema({
            name,
            description,
            price,
            user_id: req.user._id,
            cross_sell: productTagList
        });
        newProduct.save().then(promo => {
            res.redirect("/dashboard");
        })
        .catch(err => console.log(err));
    }
});

router.post('/basket/add/:id', ensureAuthenticated, (req, res) => {
    const productId = req.params.id;
    const userId = req.user._id;

    BasketSchema.findOne({user_id: userId.toString() }).then(basket => {
        if(basket) {
            const newProducts = basket.products.push(productId);
            BasketSchema.findOneAndUpdate({ user_id: userId.toString() }, { '$set': {
                'products': basket.products
                }
            }, function(err) {
                if (err) { console.log(err); } 
                else {
                    res.redirect("/market");
                }
            });
        } else {
            const newBasket = new BasketSchema({
                user_id: userId,
                products: productId,
            });
            newBasket.save().then(basket => {
                res.redirect("/market");
            })
            .catch(err => console.log(err));
        }
    });

});

///////////////////////////////////////////////////////////////////
//  UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////

// Get a product detail
router.post('/update/:id', ensureAuthenticated, (req, res) => {
    
    const { name, description, price, productTag } = req.body;

    let productTagList = productTag.toLowerCase().replace(/ /g, '').split(',');

    errors = checkFieldsUpdateProduct(req.body);


    if(errors.length > 0) {
        req.flash(errors);
        res.redirect('/product/'+req.params.id);
    } else {
        ProductSchema.findOneAndUpdate({ _id: req.params.id }, { '$set': {
            'name': name, 
            'description': description,
            'price': price,
            'cross_sell': productTagList
            } 
        }, function(err, user) {
            if (err) {
                console.log('[-] Update product info failed');
                console.log(c.info + '[i] Updating Product info terminated' + c.reset);
                req.flash('warning_msg', 'The product couldn\'t be updated...');
                res.redirect('/product/'+req.params.id);
                throw (err);
            } else {
                console.log('[-] Update user info success');
                console.log(c.info + '[i] Updating user info terminated' + c.reset);
                req.flash('success_msg', 'Product updated');
                res.redirect('/dashboard');
            }
        });
    }

});



///////////////////////////////////////////////////////////////////
//  DELETE ROUTES METHODS
///////////////////////////////////////////////////////////////////

// Delete the object
router.get('/delete/:id', ensureAuthenticated, (req, res) => {
    ProductSchema.findOneAndDelete({_id: req.params.id}).exec(function(err) {
        if(!err) {
            req.flash('success_msg','Your product was deleted');
            res.redirect('/product/view');
        } 
        else { 
            req.flash('warning_msg','An error occurred');
            console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); 
        }
    });
});

module.exports = router;