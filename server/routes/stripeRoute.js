const express = require('express');
const router = express.Router();

// Database & Models
const mongoose = require('mongoose');
const StripeModel = require('./../models/StripeModel');

// Data security libraries
const bcrypt = require('bcrypt');
const CryptoJS = require("crypto-js");

// Used to manage payment actions
const stripe = require('stripe')(process.env.Secret_Key);

// Environment variables
const env = require("dotenv").config({ path: "./.env" });

// Map controllers
const { society_info, society_address, user_info, user_address, products } = require('./../controller/databaseController.js');
const { calculateOrderAmount } = require('./../controller/purchaseController');

const c = require('../conf/colors');

// Get data sent from the client to transmit them 
// to the Stripe API
router.post('/payment', async function(req, res){
    // Map product id to product properties and calculate amount
    calculatedProduct = calculateOrderAmount(req.body.product_id);
    if(calculatedProduct != null) {
        stripe.customers.create({ 
            // Get data from Stripe fields
            email: req.body.stripeEmail, 
            source: req.body.stripeToken,
            // Get data from the front and the server 
            address: { 
                line1: user_address.line1, 
                postal_code: user_address.postal_code, 
                city: user_address.city, 
                state: user_address.state, 
                country: user_address.country
            } 
        }) 
        .then((customer) => { 
            // Product properties
            return stripe.charges.create({ 
                amount: calculatedProduct.orderAmount,
                description: calculatedProduct.product_desc, 
                currency: 'USD', 
                customer: customer.id 
            }); 
        }) 
        .then((charge) => { 
            // Insert into database the purchase after stripe validation
            StripeModel.findOne({ transactionToken: req.body.stripeToken }).then(stripe => {
                // Check if transaction already exists in database
                if (stripe) {
                    console.log(c.info + '[i] Transaction already exists' + c.reset);
                } else {
                    console.log('[-] ' + req.body.stripeToken);
                    console.log('[-] ' + charge.id);

                    const newTransaction = new StripeModel({
                        transactionToken: req.body.stripeToken,
                        chargeId: charge.id,
                        currency: charge.currency,
                        price: calculatedProduct.orderAmount / 100,
                    });

                    // Save transaction fields in database
                    newTransaction.save().then(stripe => {
                        console.log(c.info + '[i] Transaction successful' + c.reset);
                    })
                    .catch(err => console.log(c.fRed + err + c.reset));

                }
            });

            res.render('invoice', {
                user_name: user_info.name,
                user_lastname: user_info.last,
                user_email: req.body.stripeEmail,
                user_address: user_address,
                product: {
                    product_id: calculatedProduct.pid,
                    product_name: calculatedProduct.product_name,
                    product_price: calculatedProduct.orderAmount / 100,
                    product_ht: (calculatedProduct.orderAmount / 100) / 1.055,
                    product_tax: (calculatedProduct.orderAmount / 100 - (calculatedProduct.orderAmount / 100) / 1.055),
                    product_desc: calculatedProduct.product_desc
                },
                society_address: society_address,
                society_info: society_info
            });
        })
        .catch((err) => { 
            // In case of error
            console.log(c.fYellow + '[!] Transaction totally or partially failed' + c.reset);
            res.render('Error');
        }); 
    } else {
        res.send(c.fYellow + "[!] Product ID undefined" + c.reset);
    } 
});


module.exports = router;