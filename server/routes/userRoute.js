const express = require('express');
const router = express.Router();

// Database & Models
const UserSchema = require('../models/UserModel');
const ProductSchema = require('../models/ProductModel');

// Data security libraries
const bcrypt = require('bcrypt');
const CryptoJS = require("crypto-js");

// Session
const passport = require('passport');

// Environment variables
const env = require("dotenv").config({ path: "./.env" });
const c = require('../conf/colors');

// Map controllers
const { forwardAuthenticated, ensureAuthenticated } = require('../conf/passportSetup');
const { checkFieldsRegistration, checkUserInfo, checkUserAddress, checkUserAccount } = require('../controller/registrationController');



///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////

// Get the connection form
router.get('/connect', forwardAuthenticated, function(req, res){ 
    res.render('Connect', {
        navtype: req.user,
    });
});

// Get the register form
router.get('/register', forwardAuthenticated, function(req, res){ 
    res.render('Register', {
        navtype: req.user,
    });
});

// Get the user profile on the dashboard
router.get('/profile', ensureAuthenticated, (req, res) => {
    res.render('Profile', {
        navtype: req.user,
        user: req.user,
    });
});

// Get user dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) => {
    ProductSchema.find().sort({_id:-1}).exec(function(err, product) {
        if(!err) {
            res.render('Market', {
                navtype : req.user, 
                product
            });
        } 
        else { console.log(c.fYellow + '[!] Error while retrieving data' + c.reset); }
    });
});

// Get admin dashboard
router.get('/dashboard-admin', ensureAuthenticated, (req, res) => {
    res.render('AdminDashboard', {
        navtype : req.user, 
    });

});

// Logout from dashboard
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('connect');
});




///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

// Attempt to connect to the dashboard and check roles
router.post('/connect', function(req, res, next) {
    let errors = [];
    // Login user and check his role
    passport.authenticate('local', function(err, user) {
        if(err) { return next(err); }
        // If user exists
        if (user){
            // If user is invite
            if (user.role === 1) {
                console.log('[-] User role 1');
                req.logIn(user, function(err) {
                    if (err) { return next(err); }
                    return res.redirect('/dashboard');
                });
            // If user is admin
            } else if (user.role === 11) { 
                console.log('[-] User role 11');
                req.logIn(user, function(err) {
                    if (err) { return next(err); }
                    return res.redirect('/dashboard-admin');
                });
            } else {
                req.flash('error', 'Error 403 you haven\'t the right to access to this page');
                return res.redirect('/error');
          }
        // If user don't exists
        } else {
            req.flash('error_msg', 'Error 401, User doesn\'t exists, check fields or register');
            return res.redirect('/connect');
        }
      })(req, res, next);

});

// Attempt to create a new user access
router.post('/register', function(req, res){
    let errors = [];
    const { firstName, lastName, birth, email, confirmEmail,
            addressLine, addressLine2, addressPostalCode, addressCity,
            addressState, addressCountry, password, confirmPassword } = req.body;

    // Control fields validity before registration
    errors = checkFieldsRegistration(req.body);

    // If errors are occurred send errors to the front in flash messages 
    if (errors.length > 0) {
        res.render('register', {
            errors,
            firstName, lastName, birth, email, confirmEmail,
            addressLine, addressLine2, addressPostalCode, addressCity,
            addressState, addressCountry, password, confirmPassword
        });
    } else {
        // Search if user already exists before save
        UserSchema.findOne({email: email}).then(user => {
            if(user) {
                console.log('User already exists');
                errors.push({ msg: 'Email already exists' });
                res.render('register', {
                    errors,
                    firstName, lastName, birth, email, confirmEmail,
                    addressLine, addressLine2, addressPostalCode, addressCity,
                    addressState, addressCountry, password, confirmPassword
                });
            } else {
                // User is not already registered in the database so we can
                // continue the registration
                console.log(c.info + '[i] Saving user...' + c.reset);

                // Set the User schema to save in database
                // address.xxx map address sub object parameters
                const newUser = new UserSchema ({
                    firstName,
                    lastName,
                    birth,
                    email,
                    'address.line': addressLine,
                    'address.line2': addressLine2,
                    'address.postalCode': addressPostalCode,
                    'address.city': addressCity,
                    'address.state': addressState,
                    'address.country': addressCountry,
                    password,
                    salt: ''
                });


                //Generate random salt for every transaction
                let saltMin = parseInt(process.env.SALT_ROUND_MIN);
                let saltMax = parseInt(process.env.SALT_ROUND_MAX);
                let saltRound = Math.floor(Math.random() * (saltMax - saltMin)) + saltMin;
                console.log(c.info + '[i] Salt on save ' + saltRound + c.reset);


                // Salt and Crypt password before save
                bcrypt.genSalt(saltRound, (err, salt) => {
                    bcrypt.hash(newUser.password + process.env.PEPPER, salt, (err, hash) => {
                        if (err) { 
                            throw err; 
                        } else {
                            newUser.password = hash;
                            // Encrypt the salt to protect database
                            let saltEncrypt = CryptoJS.AES.encrypt(String(saltRound), process.env.AES_SECRET_KEY).toString();
                            newUser.salt = saltEncrypt;
                            
                            // Save the user into database
                            newUser.save().then(user => {
                                console.log(c.info + '[i] Saving user terminated' + c.reset);
                            })
                            .catch(err => console.log(c.fRed + err + c.reset));
                        }   
                    });
                });

                // Redirect user to the login page after registration
                res.redirect('connect');

            }
        });
    }
   
});


///////////////////////////////////////////////////////////////////
//  UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////



// Update user info from profile dashboard
router.post('/update-user-info', function(req, res) {
    const { first_name, last_name, birth_date, phone, mobile } = req.body;

    errors = checkUserInfo(req.body);


    if(errors.length > 0) {
        req.flash(errors);
        res.redirect('/profile');
    } else {
        UserSchema.findOneAndUpdate({ email: req.body.email }, { '$set': {
            'firstName': first_name, 
            'lastName': last_name,
            'birth': birth_date, 
            'phone': phone, 
            'mobile': mobile
            } 
        }, function(err, user) {
            if (err) {
                console.log('[-] Update user info failed');
                console.log(c.info + '[i] Updating user info terminated' + c.reset);
                req.flash('warning_msg', 'Your profile has failed to update');
                res.redirect('/profile');
                throw (err);
            } else {
                console.log('[-] Update user info success');
                console.log(c.info + '[i] Updating user info terminated' + c.reset);
                req.flash('success_msg', 'Your profile has been updated');
                res.redirect('/profile');
            }
        });

    }
    
});

router.post('/update-user-address', function(req, res) {
    const { addressLine1, addressLine2, postalCode, city, state, country } = req.body;

    errors = checkUserAddress(req.body);

    if(errors.length > 0) {
        req.flash(errors);
        res.redirect('/profile');
    } else {
        UserSchema.findOneAndUpdate({ email: req.body.email }, { '$set': {
            'address.line': addressLine1,
            'address.line2': addressLine2,
            'address.postalCode': postalCode,
            'address.city': city,
            'address.state': state,
            'address.country': country
            } 
        }, function(err, user) {
            if (err) {
                console.log('[-] Update user address failed');
                console.log(c.info + '[i] Updating user address terminated' + c.reset);
                req.flash('warning_msg', 'Your profile has failed to update');
                res.redirect('/profile');
                throw (err);
            } else {
                console.log('[-] Update user address success');
                console.log(c.info + '[i] Updating user address terminated' + c.reset);
                req.flash('success_msg', 'Your profile has been updated');
                res.redirect('/profile');
            }
        });

    }
    
});

router.post('/update-user-account', function(req, res) {
    const { newEmail, email, oldPassword, newPassword, confirmNewPassword } = req.body;

    errors = checkUserAccount(req.body);

    if(errors.length > 0) {
        req.flash(errors);
        res.redirect('/profile');
    } else {
        console.log(req.body);

        // Check if there is a user matching with internal email (hidden field)
        UserSchema.findOne({ email: email }).then(user => {
            if (!user) {
                console.log(c.fYellow + '[-] That email is not registered' + c.reset);
            }

            // Check if user email match with the password hash before changing
            // account parameters for security usage
            bcrypt.compare(oldPassword + process.env.PEPPER, user.password, (err, isMatch) => {
                if (err) throw err;
                
                // If the user password match do
                if (isMatch) {
                    console.log(c.info + '[i] Password match user on password update' + c.reset);
                    
                    // Get if user had changed his password with the returned verification flag 
                    if((newPassword != '') && (confirmNewPassword != '')) {

                        //Generate random salt for every transaction
                        let saltMin = parseInt(process.env.SALT_ROUND_MIN);
                        let saltMax = parseInt(process.env.SALT_ROUND_MAX);
                        let saltRound = Math.floor(Math.random() * (saltMax - saltMin)) + saltMin;
                        console.log(c.info + '[i] Salt on save ' + saltRound + c.reset);

                        // Salt and Crypt the new password before save
                        bcrypt.genSalt(saltRound, (err, salt) => {
                            bcrypt.hash(newPassword + process.env.PEPPER, salt, (err, hash) => {
                                if (err) { 
                                    throw err; 
                                } else {
                                    // Encrypt the salt to protect database
                                    let saltEncrypt = CryptoJS.AES.encrypt(String(saltRound), process.env.AES_SECRET_KEY).toString();
                                    
                                    UserSchema.findOneAndUpdate({ email: email }, { '$set': {
                                        'email': newEmail,
                                        'password': hash,
                                        'salt': saltEncrypt
                                        } 
                                    }, function(err, user) {
                                        if (err) {
                                            console.log('[-] Update user account failed');
                                            console.log(c.info + '[i] Updating user account terminated' + c.reset);
                                            req.flash('warning_msg', 'Your profile has failed to update');
                                            res.redirect('/profile');
                                            throw (err);
                                        } else {
                                            console.log('[-] Update user address success');
                                            console.log(c.info + '[i] Updating user account terminated' + c.reset);
                                            req.flash('success_msg', 'Your profile has been updated');
                                            res.redirect('/profile');
                                        }
                                    });
                                }   
                            });
                        });

                    
                    // If nothing had changed or only email
                    } else {
                        UserSchema.findOneAndUpdate({ email: email }, { '$set': {
                            'email': newEmail
                            } 
                        }, function(err, user) {
                            if (err) {
                                console.log('[-] Update user account failed');
                                console.log(c.info + '[i] Updating user account terminated' + c.reset);
                                req.flash('warning_msg', 'Your profile has failed to update');
                                res.redirect('/profile');
                                throw (err);
                            } else {
                                console.log('[-] Update user address success');
                                console.log(c.info + '[i] Updating user account terminated' + c.reset);
                                req.flash('success_msg', 'Your profile has been updated');
                                res.redirect('/profile');
                            }
                        });
                    }

                } else {
                     console.log(c.fYellow + '[-] Password didn\'t match user for update' + c.reset);
                     req.flash('warning_msg', 'Your old password is wrong');
                     res.redirect('/profile');
                }
            });
        });       

    }
    
});


module.exports = router;
